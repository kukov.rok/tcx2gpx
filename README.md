# tcx2gpx

This module converts the Garmin [tcx](https://en.wikipedia.org/wiki/Training_Center_XML) GPS file format
to the more commonly used [gpx](https://en.wikipedia.org/wiki/GPS_Exchange_Format) file format.
Both formats are a form of [XML](https://en.wikipedia.org/wiki/XML) but there are some fields in the former that are not
present in the later.
It uses two packages to do the grunt work [tcxparser](https://github.com/vkurup/python-tcxparser/) and
[gpxpy](https://github.com/tkrajina/gpxpy).

## Installation

Install from [PyPi.org](https://pypi.org/project/tcx2gpx) using...

```bash
pip install tcx2gpx
```

For more information on installing please refer to the [documentation]().

## Usage

The easiest way to use `tcx2gpx` is the command line version included. It will be default search the current directory
path for files with the `.tcx` extension and convert them to `.gpx`. There are various options available that change
where output is written or the directory that is searched. For details on usage see...

``` bash
tcx2gpx --help
```

For more on information see the [documentation]().
